# Vehicle tracking service

## Quick start

This project will require Node.js and NPM to be installed.

Install dependencies listed in package.json:

```
npm install
```

This service requires access to a firebase database, add the path to your firebase account key json file in the app.js file on line 6

```
// Add the path to your firebase adminsdk json file here
const serviceAccount = require("../path/to/drivertracker-7f17b-firebase-adminsdk-tcl2h-953a9203c5.json")
```

To start the server:

```
node app.js
```
# API

**GET: /vehicles**

Returns an object with vehicle id key to object pairs, with the object value containing geolocation information.

eg: GET http://localhost:4000/vehicles

Returns

```
{
    "success": true,
    "result": { 
        "abc123":
        { 
            "lat": 10,
            "lng": 20,
            "at": "2019-11-19T11:43:57.506Z"
        }
    }
}
```
**Post: /vehicles/:id/locations**

Creates a new location object in the main location database and updates or adds a new object to the latest locations database

eg: GET http://localhost:4000/vehicles/abc123/locations

Request body:

```
{ 
    "lat": 10.0, 
    "lng": 20.0, 
    "at": "2017-09-01T12:00:00Z" 
}
```

Database object:

```
{
    "abc123":
    { 
        "lat": 10,
        "lng": 20,
        "at": "2019-11-19T11:43:57.506Z"
    }
}
```

**POST: /vehicles**

Creates a new vehicle item in the database with an auto generated id and that object contains the id sent in the POST body JSON, a timestamp and a register type.

eg: POST http://localhost:4000/vehicles

Request body:

```
{ 
    "id": "abc123" 
}
```

Database object:

```
{ 
    "fb123":
    { 
        at: "2019-11-19T11:43:57.506Z",
        id: "abc123",
        type: "register" 
    }
}
```


**DELETE: /vehicles/:id**

Deregisters a vehicle item in the register database with an auto generated id and that object contains the id sent in the POST body JSON, a timestamp and a register type.

eg: DELETE http://localhost:4000/vehicles/abc123

Database object:

```
{ 
    "fb123":
    { 
        at: "2019-11-19T11:43:57.506Z",
        id: "abc123",
        type: "deregister" 
    }
}
```

## Things I would like to have had the time to do:
- Add testing
- Add some error logging for the backend
- Handle removing things from the database better
- Get direction vehicle is travelling
- Make the front end look nicer
- Group nearby vehicles and adjust marker appearance on zoom
- Clean up the backend code, put things into objects and separate into files
- Figure out how to deal with access tokens more elegantly
- Setup up a DockerFile
- Host on a website
