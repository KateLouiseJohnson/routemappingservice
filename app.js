const express = require('express')
const admin = require('firebase-admin')
const bodyParser = require('body-parser')
const path = require('path')
// Add the path to your firebase adminsdk json file here
const serviceAccount = require('../path/to/drivertracker-7f17b-firebase-adminsdk-tcl2h-953a9203c5.json')

// Server
const port = 4000
const centerLocation = { lat: 52.53, lng: 13.403 }
const app = new express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static(path.join(__dirname, 'landing')))

// Database
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://drivertracker-7f17b.firebaseio.com'
})

const db = admin.database()
const ref = db.ref('restricted_access/secret_document')
const locationsRef = ref.child('locations')
const vehicleRegisterRef = ref.child('vehicleRegister')
const latestLocationsRef = ref.child('latestLocations')
latestLocationsRef.set({})

// register vehicle location
app.post('/vehicles/*/locations', (req, res) => {
    try
    {
        const json = req.body
        const id = tryGetIdFromPath(req.path)
        const lat = json.lat
        const lng = json.lng
        const at = new Date().toISOString()
        const isInRange = isWithinRange(lat, lng)
        
        if (isInRange) {
            // store all data for analysis
            // use of push creates a unique key per entry
            locationsRef.push({
                lat,
                lng,
                at,
                id,
            })

            // store most recent position for easy access
            // use of child method creates/overwrites child with given id
            latestLocationsRef.child(id).set({
                lat,
                lng,
                at,
            })
        } else {
            // remove out of bounds vehicle from latest location results
            // could be more efficient
            latestLocationsRef.orderByKey().equalTo(id).once('value', (snap) => {
                // check if the child exists in the database
                if (snap.exists()) {
                    latestLocationsRef.child(id).remove()
                }
            })
        }

        // I feel like success equals true is still okay if they were out of bounds
        res.json({ success: true })
    }
    catch (error)
    {
        console.log('Location log failed: ' + error)
        res.json({ success: false, error })
    }   
})

// register vehicle
app.post('/vehicles', (req, res) => {
    try
    {
        const json = req.body
        const id = json.id
        const at = new Date().toISOString()

        // Log full data here for Data Science
        // use of push creates a unique key per entry
        vehicleRegisterRef.push({
            id,
            at,
            type: 'register'
        })

        res.json({ success: true })
    }
    catch (error)
    {
        console.log('Registration failed: ' + error)
        res.json({ success: false, error })
    }
})

// deregister vehicle
app.delete('/vehicles/*', (req, res) => {
    try
    {
        const id = tryGetIdFromPath(req.path)
        const at = new Date().toISOString()

        // store all data for analysis
        // use of push creates a unique key per entry
        vehicleRegisterRef.push({
            id,
            at,
            id,
        })
        
        // remove vehicle from latest location results
        // could maybe be more efficient
        latestLocationsRef.orderByKey().equalTo(id).once('value', (snap) => {
            // check if the child exists in the database
            if (snap.exists()) {
                latestLocationsRef.child(id).remove()
            }
        })

        res.json({ success: true })
    }
    catch (error)
    {
        console.log('Deregistration failed: ' + error)
        res.json({ success: false, error })
    }
})

app.get('/vehicles', async (req, res) => {
    try {
        const locData = await getLocationData()        
        res.json({ success: true, result: locData })
    }
    catch (error) 
    {
        res.json({ success: false, error })
    }
})

app.listen(port, () => {
    console.log('server listening on ' + port)
})

async function getLocationData() {
    let result = {}
    
    await latestLocationsRef.once('value', async (snap) => {
        if (snap.exists()) {
            const container = {}
            snap.forEach((childSnap) => {
                const child = childSnap.val()
                const id = childSnap.key
                const lat = child.lat
                const lng = child.lng
                const at = child.at
        
                container[id] = {
                    at,
                    lat,
                    lng,
                }
            })
            
            result = container
        }
    })

    return result
}

function tryGetIdFromPath(path) {
    const split = path.split('/')
    if(split.length >= 3) {
        return split[2]
    }
    console.log('Failed to find id in path: ' + path)

    // don't think this would actually ever happen
    return 'unknown'
}

function isWithinRange(lat, lng) {
    const dist = getDistanceFromCenter(lat, lng)
    return dist < 3.5
}

function degreesToRadians(degrees) {
    // credit where credit is due
    // https://stackoverflow.com/questions/365826/calculate-distance-between-2-gps-coordinates
    return degrees * Math.PI / 180
}
  
function getDistanceFromCenter(lat, lng) {
    // credit where credit is due
    // https://stackoverflow.com/questions/365826/calculate-distance-between-2-gps-coordinates
    const earthRadiusKm = 6371

    const dLat = degreesToRadians(centerLocation.lat - lat)
    const dLon = degreesToRadians(centerLocation.lng - lng)

    rLat = degreesToRadians(lat)
    rLat2 = degreesToRadians(centerLocation.lat)

    const a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.sin(dLon/2) * Math.sin(dLon/2) * 
            Math.cos(rLat) * Math.cos(rLat2) 
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))
    return earthRadiusKm * c
}
