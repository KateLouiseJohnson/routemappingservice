let map = {}
const markers = {}

class App {
    static init() {
        map = L.map('driverMap').setView([52.53, 13.403], 13)
        const token = 'pk.eyJ1Ijoia2F0ZWxvdWlzZWpvaG5zb24iLCJhIjoiY2szMjVxeGdhMGdncjNkcGJ0cGxsOXRocyJ9.LmF22qF3NehccYPDQt7uNQ';

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + token, {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: token
        }).addTo(map)
    }

    static run() {
        if (document.readyState != 'loading') {
            App.init()
        } else if (document.addEventListener) {
            document.addEventListener('DOMContentLoaded', App.init())
        } else {
            document.attachEvent('onreadystatechange', () => {
                if (document.readyState === 'complete') App.init()
            })
        }
        
        setInterval(() => {
            App.getData()
        }, 3000)
    }

    static updateMarkers(vehicleLocations) {
        const ids = []

        for (let id in vehicleLocations) {
            ids.push(id)
            const data = vehicleLocations[id]
            this.updateMarker(id, data)
        }

        for (let id in markers) {
            // remove marker as id did not come through with last request
            if (!ids.includes(id)) {
                map.removeLayer(markers[id].marker)
            }
        }
    }

    static updateMarker(id, vehicle) {
        const lat = vehicle.lat
        const lng = vehicle.lng
        let marker

        if (!markers[id]) {
            marker = L.circle([lat, lng], {
                color: 'red',
                fillColor: '#f03',
                fillOpacity: 0.5,
                radius: 50
            }).bindPopup(id).addTo(map)
        } else {
            marker = markers[id].marker
            marker.setLatLng([lat, lng])
        }

        markers[id] = {
            marker,
            lat,
            lng,
        }
    }

    static getData() {

        const promise = new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest()
            xhr.open("GET", '/vehicles')
            xhr.onload = () => resolve(xhr.responseText)
            xhr.onerror = () => reject(xhr.statusText)
            xhr.send()
        })

        promise.then((res) => {
            const responseData = JSON.parse(res)
            if (Object.keys(responseData.result).length > 0) {
                App.updateMarkers(responseData.result)
            }
        })
    }
}

App.run()
